export const MIN_FAMILY_NAME_LENGTH = 2;
export const MAX_FAMILY_NAME_LENGTH = 30;
export const CHECK_IN_STORAGE_KEY = 'checkedIn';