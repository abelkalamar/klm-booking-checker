import { Injectable } from '@angular/core';
import { CanActivate, Router, UrlTree } from '@angular/router';
import { CHECK_IN_STORAGE_KEY } from 'src/shared/constants/app.constants';

@Injectable({
  providedIn: 'root'
})
export class BookingGuard implements CanActivate {

  constructor(private router: Router) { }

  canActivate(): boolean {
    if (localStorage.getItem(CHECK_IN_STORAGE_KEY) === 'true') {
      return true;
    } else {
      this.router.navigate(['/check-in']);
      return false;
    }
  }
}
