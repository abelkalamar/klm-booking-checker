import { InjectionToken } from '@angular/core';
import { Config } from './types/config.interface';

export const CONFIG_TOKEN = new InjectionToken<Config>('CONFIG_TOKEN');
