import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CONFIG_TOKEN } from './config.token';
import { Config } from './types/config.interface';
import { BehaviorSubject, Observable, of, tap } from 'rxjs';
import { Booking } from './types/booking';

@Injectable({ providedIn: 'root' })
export class ApiService {

  // this should come from the backend
  private booking$ = new BehaviorSubject<Booking | undefined>({
    bookingCode: "PZIGZ3",
    contactDetails: "TRAINER@YAHOO.FR",
    itinerary: {
      type: "ONE_WAY",
      connections: [
        {
          id: 1,
          duration: "120",
          origin: {
            IATACode: "AMS",
            name: "Schiphol",
            city: {
              IATACode: "AMS",
              name: "Amsterdam",
              country: {
                code: "NL",
                name: "The Netherlands"
              }
            }
          },
          destination: {
            IATACode: "NCE",
            name: "Cote D'Azur Airport",
            city: {
              IATACode: "NCE",
              name: "Nice",
              country: {
                code: "FR",
                name: "France"
              }
            }
          },
          segments: [
            {
              id: 2,
              type: "LOCAL",
              informational: false,
              departFrom: {
                IATACode: "AMS",
                name: "Schiphol",
                city: {
                  IATACode: "AMS",
                  name: "Amsterdam",
                  country: {
                    code: "NL",
                    name: "The Netherlands"
                  }
                }
              },
              arriveOn: {
                IATACode: "NCE",
                name: "Cote D'Azur Airport",
                city: {
                  IATACode: "NCE",
                  name: "Nice",
                  country: {
                    code: "FR",
                    name: "France"
                  }
                }
              },
              marketingFlight: {
                number: "1263",
                carrier: {
                  code: "KL",
                  name: "KLM"
                },
                status: {
                  code: "CONFIRMED",
                  name: "Confirmed"
                },
                numberOfStops: 0,
                sellingClass: {
                  code: "Z"
                },
                operatingFlight: {
                  number: "1263",
                  carrier: {
                    code: "KL",
                    name: "KLM"
                  },
                  duration: "PT2H",
                  flown: false,
                  checkInStart: "2016 - 10 - 13T03: 35 + 02: 00",
                  localCheckInStart: "2016 - 10 - 13T03: 35",
                  checkInEnd: "2016 - 10 - 14T08: 35 + 02: 00",
                  localCheckInEnd: "2016 - 10 - 14T08: 35",
                  scheduledArrival: "2016 - 10 - 14T11: 35 + 02: 00",
                  localScheduledArrival: "2016 - 10 - 14T11: 35",
                  scheduledDeparture: "2016 - 10 - 14T09: 35 + 02: 00",
                  localScheduledDeparture: "2016 - 10 - 14T09: 35",
                  arrivalTerminal: {
                    name: "2"
                  },
                  cabin: {
                    code: "10",
                    name: "Business"
                  },
                  equipment: {
                    code: "73H",
                    name: "Boeing 737-800"
                  }
                }
              }
            }
          ]
        }
      ]
    },
    passengers: {
      id: 1,
      firstName: "RUUD",
      lastName: "HESP",
    }
  });

  public getBooking$(): Observable<Booking | undefined> {
    return this.booking$.asObservable()
  }

  public constructor(
    @Inject(CONFIG_TOKEN) private readonly config: Config,
    private readonly http: HttpClient,
  ) { }

  public validateCheckInCredentials$(bookingCode: string, familyName: string): Observable<boolean> {
    // TODO validate bookingCode and familyName on the backend (optional)
    return of(true);
  }

  public fetchBooking$(): void {
    const query = `{ 
      booking {
        bookingCode
        contactDetails
      }
    }`;
    this.http.post(this.config.apiBaseUrl, JSON.stringify({ query: query }), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }).subscribe(result => {
      this.booking$.next(result as Booking)
    });
  }

}