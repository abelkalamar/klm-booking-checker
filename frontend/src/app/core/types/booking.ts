export interface Booking {
  bookingCode: string;
  contactDetails: string;
  itinerary: Itinerary,
  passengers: Passenger
}

interface Itinerary {
  [key: string]: any; // TODO
}

interface Passenger {
  [key: string]: any; // TODO
}
