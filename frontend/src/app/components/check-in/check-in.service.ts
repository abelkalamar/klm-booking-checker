import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/core/api.service';
import { filter, take, tap } from 'rxjs/operators'
import { Router } from '@angular/router';

@Injectable()
export class CheckInService {

  constructor(private readonly apiService: ApiService, private readonly router: Router) { }

  public checkIn(bookingCode: string, familyName: string): void {
    this.apiService.validateCheckInCredentials$(bookingCode, familyName)
      .pipe(filter(credentialsAreValid => credentialsAreValid), take(1)).subscribe(() => {
        localStorage.setItem('checkedIn', 'true');
        this.router.navigate(['/booking']);
      });
  }
}