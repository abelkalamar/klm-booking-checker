import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { CheckInService } from './check-in.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ApiService } from 'src/app/core/api.service';
import { MAX_FAMILY_NAME_LENGTH, MIN_FAMILY_NAME_LENGTH } from 'src/shared/constants/app.constants';


export interface CheckInFormVM {
  bookingCode: FormControl<string>;
  familyName: FormControl<string>;
}

@Component({
  selector: 'app-check-in',
  templateUrl: './check-in.component.html',
  styleUrls: ['./check-in.component.scss'],
  providers: [CheckInService],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CheckInComponent implements OnInit {
  public checkInFormGroup!: FormGroup<CheckInFormVM>;

  public get bookingCodeControl(): FormControl<string> {
    return this.checkInFormGroup.controls['bookingCode'] as FormControl;
  }

  public get familyNameControl(): FormControl<string> {
    return this.checkInFormGroup.controls['familyName'] as FormControl;
  }
  public constructor(private readonly formBuilder: FormBuilder, private readonly checkInService: CheckInService) { }

  public ngOnInit(): void {
    this.checkInFormGroup = this.initForm();
  }

  public onSubmit(): void {
    if (this.checkInFormGroup.invalid) {
      return;
    }
    const { bookingCode, familyName } = this.checkInFormGroup.value;
    this.checkInService.checkIn(bookingCode as string, familyName as string);
    this.checkInFormGroup.reset();
  }

  private initForm(): FormGroup {
    return this.formBuilder.group({
      bookingCode: this.formBuilder.control('',
        [Validators.required, this.bookingCodeValidator]),
      familyName: this.formBuilder.control('',
        [Validators.required, Validators.minLength(MIN_FAMILY_NAME_LENGTH), Validators.maxLength(MAX_FAMILY_NAME_LENGTH)]),
    });
  }

  private bookingCodeValidator(control: FormControl): { [s: string]: boolean } | null {
    const pattern = /^[2-9a-zA-Z]{5,6}$/;
    if (!pattern.test(control.value)) {
      return { 'bookingCodeIsInvalid': true }
    }
    return null;
  }

}
