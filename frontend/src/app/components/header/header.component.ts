import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class HeaderComponent implements OnInit {
  public pageTitle!: string;

  constructor(private readonly location: Location) { }

  public ngOnInit(): void {
    this.pageTitle = this.location.path().slice(1);
  }
}
