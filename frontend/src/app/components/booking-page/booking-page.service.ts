import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/core/api.service';
import { Observable } from 'rxjs';
import { Booking } from 'src/app/core/types/booking';

@Injectable()
export class BookingPageService {
  public booking$: Observable<Booking | undefined> = this.apiService.getBooking$();

  constructor(private readonly apiService: ApiService) { }

  public fetchBooking(): void {
    this.apiService.fetchBooking$();
  }
}