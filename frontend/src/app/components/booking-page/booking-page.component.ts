import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { BookingPageService } from './booking-page.service';

@Component({
  selector: 'app-booking-page',
  templateUrl: './booking-page.component.html',
  styleUrls: ['./booking-page.component.scss'],
  providers: [BookingPageService],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BookingPageComponent implements OnInit {
  public booking$ = this.bookingPageService.booking$;

  constructor(private readonly bookingPageService: BookingPageService) { }

  public ngOnInit(): void {
    this.bookingPageService.fetchBooking();
  }

}
