import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'objectValue'
})
export class ObjectValuePipe implements PipeTransform {
  transform(value: { [key: string]: number | string }): string {
    if (!value) return value;
    return Object.values(value)[0].toString();
  }
}