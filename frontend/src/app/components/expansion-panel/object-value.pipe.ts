import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'objectKey'
})
export class ObjectKeyPipe implements PipeTransform {
  transform(value: { [key: string]: number | string }): string {
    if (!value) return value;
    return Object.keys(value)[0];
  }
}