import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-expansion-panel',
  templateUrl: './expansion-panel.component.html',
  styleUrls: ['./expansion-panel.component.scss']
})
export class ExpansionPanelComponent implements OnInit {
  @Input() title: string | undefined;
  @Input() data: { [key: string]: number | string } | undefined;
  public dataToDisplay: { [key: string]: number | string }[] = []

  public ngOnInit(): void {
    if (!this.data) {
      return;
    }
    Object.keys(this.data).forEach(key => {
      let value;
      if (typeof this.data![key] !== 'string' && typeof this.data![key] !== 'number') {
        value = 'Further mapping required'
      } else {
        value = this.data![key];
      }
      this.dataToDisplay.push({ [key]: value })
    })
  }
}
