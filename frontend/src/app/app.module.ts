import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatExpansionModule } from '@angular/material/expansion';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { CheckInComponent } from './components/check-in/check-in.component';
import { BookingPageComponent } from './components/booking-page/booking-page.component';
import { CardComponent } from './components/card/card.component';
import { ExpansionPanelComponent } from './components/expansion-panel/expansion-panel.component';
import { ObjectKeyPipe } from './components/expansion-panel/object-value.pipe';
import { ObjectValuePipe } from './components/expansion-panel/object-key.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    CheckInComponent,
    BookingPageComponent,
    CardComponent,
    ExpansionPanelComponent,
    ObjectKeyPipe,
    ObjectValuePipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatExpansionModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
