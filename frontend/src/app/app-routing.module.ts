import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CheckInComponent } from './components/check-in/check-in.component';
import { BookingPageComponent } from './components/booking-page/booking-page.component';
import { BookingGuard } from './core/guards/booking.guard';

const routes: Routes = [
  {
    path: '', redirectTo: '/check-in', pathMatch: 'full'
  },
  { path: 'check-in', component: CheckInComponent },
  { path: 'booking', component: BookingPageComponent, canActivate: [BookingGuard] },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
