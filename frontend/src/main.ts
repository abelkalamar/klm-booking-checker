import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { CONFIG_TOKEN } from './app/core/config.token';
import { Config } from './app/core/types/config.interface';

document.addEventListener('DOMContentLoaded', async (): Promise<void> => {
  const response = await fetch('/assets/config/config.json');
  const config: Config = await response.json();
  platformBrowserDynamic([
    {
      provide: CONFIG_TOKEN,
      useValue: config,
    },
  ])
    .bootstrapModule(AppModule)
    .catch((error: Error): void => console.error(error));
});

