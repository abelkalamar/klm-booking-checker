import express from 'express';
import cors from 'cors';
import dotenv from 'dotenv';
import { graphqlHTTP } from 'express-graphql';
import { schema } from './graphql/schema';
import { rootValue } from './graphql/resovers';

dotenv.config();

const app = express();
const port = process.env.PORT;

app.use(cors());

app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: rootValue,
  graphiql: true
}));

app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${port}/graphql`);
});